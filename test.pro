include(tracking.pro)

CONFIG += console
CONFIG -= app_bundle

TARGET = test
TEMPLATE = app

DEFINES += _TEST

GMOCKPATH = ../gmock-1.7.0
GTESTPATH = ../gmock-1.7.0/gtest

INCLUDEPATH += src \
               test \
               $$GTESTPATH \
               $$GTESTPATH/include \
               $$GMOCKPATH \
               $$GMOCKPATH/include

DEFINES += QT_NO_KEYWORDS \
           _WCHAR_H_CPLUSPLUS_98_CONFORMANCE_

# We need to QT_NO_KEYWORDS because of forever def in gmock
# Manually define slots etc.
DEFINES += "slots=Q_SLOTS"
DEFINES += "signals=Q_SIGNALS"
DEFINES += "emit=Q_EMIT"

SOURCES -= src/main.cpp

GMOCK_HEADERS = $$GMOCKPATH/include/gmock/*.h \
                $$GMOCKPATH/include/gmock/internal/*.h
                
GTEST_HEADERS = $$GTESTPATH/include/gtest/*.h \
                $$GTESTPATH/include/gtest/internal/*.h

HEADERS += $$GTEST_HEADERS \
           $$GMOCK_HEADERS

SOURCES += $$GMOCKPATH/src/gmock-all.cc \
           $$GTESTPATH/src/gtest-all.cc \
           $$GMOCKPATH/src/gmock_main.cc \
           test/*_test.cpp
