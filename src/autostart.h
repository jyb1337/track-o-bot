#pragma once

class Autostart
{
public:
  bool IsActive();
  void SetActive(bool active);
};
